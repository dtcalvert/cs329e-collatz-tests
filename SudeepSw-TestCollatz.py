#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "10 1\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  10)
        self.assertEqual(j, 1)

    def test_read_3(self):
        s = "1000 100000\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 100000)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    def test_eval_6(self):
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)

    def test_eval_7(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_8(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_9(self):
        v = collatz_eval(100, 100)
        self.assertEqual(v, 26)

    def test_eval_10(self):
        v = collatz_eval(1000, 1000)
        self.assertEqual(v, 112)

    def test_eval_11(self):
        v = collatz_eval(100, 10000)
        self.assertEqual(v, 262)

    def test_eval_12(self):
        v = collatz_eval(1000, 100000)
        self.assertEqual(v, 351)

    def test_eval_13(self):
        v = collatz_eval(100000, 1000000)
        self.assertEqual(v, 525)


    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 10, 1, 20)
        self.assertEqual(w.getvalue(), "10 1 20\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 1000, 100000, 351)
        self.assertEqual(w.getvalue(), "1000 100000 351\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")

    def test_solve_2(self):
        r = StringIO("10 1\n200 100\n210 201\n1000 900\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "10 1 20\n200 100 125\n210 201 89\n1000 900 174\n")

    def test_solve_3(self):
        r = StringIO("1 1\n100 100\n1000 1000\n1000 100000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 1 1\n100 100 26\n1000 1000 112\n1000 100000 351\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage-3.5 run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
......................
----------------------------------------------------------------------
Ran 22 tests in 15.589s

OK


% coverage-3.5 report -m                   >> TestCollatz.out



% cat TestCollatz.out
......................
----------------------------------------------------------------------
Ran 22 tests in 15.589s

OK
Name             Stmts   Miss  Cover   Missing
----------------------------------------------
Collatz.py          63      0   100%
TestCollatz.py      87      0   100%
----------------------------------------------
TOTAL              150      0   100%
"""
